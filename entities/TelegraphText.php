<?php

    //namespace Telegraph;

    class TelegraphText
        {
            private $title;
            private $text;
            private $author;
            private $published;
            private $slug;

            public function __construct($author, $slug)
            {
                $this->setAuthor($author);
                $this->setSlug($slug);
                $this->setPublished(date("D - d - F - Y"));
            }

            public function __get($name)
            {
                if ($name == 'author') {
                    return $this->getAuthor();
                } elseif ($name == 'published') {
                    return $this->getPublished();
                } elseif ($name == 'slug') {
                    return $this->getSlug();
                } elseif ($name == 'text') {
                    return $this->loadText();
                }
            }

            public function __set($name, $value)
            {
                if ($name == 'author') {
                    return $this->setAuthor($value);
                } elseif ($name == 'published') {
                    return $this->setPublished($value);
                } elseif ($name == 'slug') {
                    return $this->setSlug($value);
                } elseif ($name == 'text') {
                    if (strlen($value) < 10 || strlen($value) > 500) {
                        throw new Exception('Текст должен быть в пределах от 1 до 500 символов.');
                        function errorHandler($level, $msg, $line, $file)
                        {
                            if($level === E_NOTICE) {
                                echo '<div style="background-color: #ffcccc; padding: 10px; border: 1px solid #ff0000; font-weight: bold;">' . $msg . '</div>';
                                $date = new \DateTime();
                                file_put_contents('admin.log', $date->format('D - m - Y, H:i:s') . ':' . $msg . ' in line ' . $line . ' in file - '. $file . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
                    $this->text = $value;
                    return $this->storeText();
                }
            }



            public function getAuthor()
            {
                return $this->author;
            }

            public function setAuthor($author)
            {
                if (strlen($author) <= 120) {
                    $this->author = $author;
                }
            }

            public function getPublished()
            {
                return $this->published;
            }

            public function setPublished($published)
            {
                if ($published >= date("D - d - F - Y")) {
                    $this->published = $published;
                }
            }

            public function getSlug()
            {
                return $this->slug;
            }

            public function setSlug($slug)
            {
                if (preg_match('/^[a-zA-Z0-9_.-]*$/', $slug)) {
                    $this->slug = $slug;
                }
            }

            private function storeText()
            {
                $textStorage = array(
                    'title' => $this->title,
                    'author' => $this->author,
                    'text' => $this->text,
                    'published' => $this->published
                );

                file_put_contents($this->slug, serialize($textStorage));

            }

            private function loadText()
            {
                if (file_exists($this->slug)) {

                    $tempArray = unserialize(file_get_contents($this->slug));

                    $this->title = $tempArray['title'];
                    $this->author = $tempArray['author'];
                    $this->text = $tempArray['text'];
                    $this->published = $tempArray['published'];

                    return $this->text;

                }

                return false;

            }

            public function editText($title, $text)
            {

                $this->title = $title;
                $this->text = $text;

            }
        }