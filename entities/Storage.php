<?php

    require_once __DIR__ . '/../interfaces/EventListenerInterface.php';
    require_once __DIR__ . '/../interfaces/LoggerInterface.php';

    abstract class Storage implements LoggerInterface, EventListenerInterface {

            abstract public function create($object);
            abstract public function read($id);
            abstract public function update($id, $object);
            abstract public function delete($id);
            abstract public function list();

            public function logMessage($errorMassage)
            {
                // TODO: Implement logMessage() method.
            }
            public function lastMessages($numberOfMessages)
            {
                // TODO: Implement lastMessages() method.
            }
            public function attachEvent($className, $callback)
            {
                // TODO: Implement attachEvent() method.
            }
            public function detouchEvent($className)
            {
                // TODO: Implement detouchEvent() method.
            }
        }