<?php

    require_once 'autoload.php';
    //require_once __DIR__ . '/../../php/vendor/autoload.php';

    //use Telegraph\TelegraphText;
    //use StorageFile\FileStorage;
    use PHPMailer\PHPMailer\PHPMailer as PHPMailer;
    use PHPMailer\PHPMailer\SMTP as SMTP;
    use PHPMailer\PHPMailer\Exception as Exception;

    if (isset($_POST['text']) && isset($_POST['author'])) {
        $text = $_POST['text'];
        $author = $_POST['author'];
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $slug = 'text.txt';

        $telegraphText = new TelegraphText($author, $slug);
        $telegraphText->setAuthor($author);
        $telegraphText->__set('text', $text);
        $telegraphText->__set('slug', $slug);

        $fileStorage = new FileStorage();
        $fileStorage->create($telegraphText);

        echo "Ваш текст сохранён в Telegraph.";

        $mail = new PHPMailer(true);

        if (!empty($email)) {
            try {
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'alexud2007@gmail.com';
                $mail->Password = 'your-password';
                $mail->SMTPSecure = 'tls';
                $mail->Port = 587;
                $mail->setFrom('alexud2007@gmail.com', 'Алексей Удиванов');
                $mail->addAddress($email);
                $mail->Subject = 'Ваш текст Telegraph';
                $mail->Body = $text;
                $mail->send();
            } catch (Exception $e) {
                echo '<div style="color: red;">Ошибка отправки email: ' . $mail->ErrorInfo . '</div>';
            } finally {
                echo '<div style="color: green;">Текст сохранен в Telegraph и отправлен на почту ' . $email . '!</div>';
            }
        }


        set_error_handler('errorHandler');
}



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Опубликовать текст в Telegraph</title>
</head>
<body>
<h1>Отправляйте свой текст в Telegraph</h1>
<div id="message"></div>
<form method="POST" action="input_text.php">
    <label for="author">Имя автора: </label>
    <input type="text" id="author" name="author"><br><br>
    <label for="text">Текст: </label>
    <textarea id="text" name="text"></textarea><br><br>
    <label for="email">Email: </label>
    <input type="text" id="email" name="email"><br><br>
    <input type="submit" value="Отправить">
</form>
</body>
</html>
