<?php

    function autoloader($className)
    {
        $path = __DIR__ . '/entities/' . $className . '.php';
        if (file_exists($path)) {
            require_once $path;
        }
    }

    spl_autoload_register('autoloader');

	require_once __DIR__ . '/../../php/vendor/autoload.php';