<?php

        interface EventListenerInterface {
            public function attachEvent($className, $callback);
            public function detouchEvent($className);
        }